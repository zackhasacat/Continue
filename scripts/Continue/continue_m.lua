local storage = require('openmw.storage')
local ui = require('openmw.ui')
local util = require('openmw.util')
local async = require('openmw.async')
local I = require('openmw.interfaces')
local ambient = require("openmw.ambient")
local input = require("openmw.input")
local modStorage = storage.playerSection('AutoLoad')

local menu = require('openmw.menu')
local buttons = {
    "menu_continue",
    "menu_newgame",
    "menu_loadgame",
    "menu_options",
    "menu_credits",
    "menu_exitgame"
}
local buttonNormalResources = {}
local buttonOverResources = {}
local buttonPressedResources = {}
for index, value in ipairs(buttons) do
    buttonNormalResources[value] = ui.texture({ path = "Textures/" .. value .. ".dds" })
    buttonOverResources[value] = ui.texture({ path = "Textures/" .. value .. "_over.dds" })
    buttonPressedResources[value] = ui.texture({ path = "Textures/" .. value .. "_pressed.dds" })
end
local buttonUIs = {}

local buttonSize = util.vector2(128, 64)

local function loadMostRecent()
    local mostRecentSave = nil
    local mostRecentTime = 0
    for folder, saveList in pairs(menu.getAllSaves()) do
        for name, save in pairs(saveList) do
            if not mostRecentSave or save.creationTime > mostRecentTime then
                mostRecentSave = { folder = folder, name = name, creationTime = save.creationTime }
                mostRecentTime = save.creationTime
            end
        end
    end

    if mostRecentSave then
        print(mostRecentSave.creationTime, "most recent", mostRecentSave.folder, mostRecentSave.name)
        menu.loadGame(mostRecentSave.folder, mostRecentSave.name)
    else
        print("No saves found.")
    end
end
local function destroyButtons()
    for index, value in ipairs(buttonUIs) do
        if value then
            value:destroy()
        end
    end
    buttonUIs = {}
end
local function onConsoleCommand(mode, command)
    if command == "load" then
        loadMostRecent()
    end
end
local function imageContent(resource, buttonName)
    local size = buttonSize
    if buttonName == "menu_continue" then
        size = util.vector2(256, 64)
    end
    return {
        type = ui.TYPE.Image,
        props = {
            resource = resource,
            size = size,
            --    anchor = util.vector2(0.5, 0.5),
        }
    }
end
local function focusLoss(x, UI)
    local id = UI.id
    UI.content[1].content[1].props.resource = buttonNormalResources[id]
    for index, value in ipairs(buttonUIs) do
        if value.layout.id == id then
            value:update()
        end
    end
end
local function mouseMove(event, UI)
    local id = UI.id
    if UI.content[1].content[1].props.resource == buttonOverResources[id] then
        return
    end
    UI.content[1].content[1].props.resource = buttonOverResources[id]

    for index, value in pairs(buttonUIs) do
        if value.layout.id == id then
            print(id)
            value:update()
        end
    end
end
local function mousePress(event, UI)
    local id = UI.id
    UI.content[1].content[1].props.resource = buttonPressedResources[id]
    for index, value in ipairs(buttonUIs) do
        if value.layout.id == id then
            value:update()
        end
    end
end
local function mouseRelease(event, UI)
    local id = UI.id
    ambient.playSoundFile("sound/fx/menu click.wav")
    UI.content[1].content[1].props.resource = buttonNormalResources[id]
    for index, value in ipairs(buttonUIs) do
        if value.layout.id == id then
            value:update()
        end
    end
    if id == "menu_continue" then
        
    destroyButtons()
    loadMostRecent()
    end
end
local function drawButtons()
    if menu.getState() ~= menu.STATE.NoGame then
        destroyButtons()
        return
    end
    destroyButtons()
    local contentItems = {}
    local layerSize = ui.layers[1].size
    for index, value in ipairs(buttons) do

        local size = 128
        if value == "menu_continue" then
            size = 64
        end
        local newButton =  {
            type = ui.TYPE.Container,
            id = value,
          --  template = I.MWUI.templates.box,
            props = {
                --  size = util.vector2(100, 100),
                position = util.vector2(128, (48 * index) ),
                anchor = util.vector2(0.5, 0),
            },
            content = ui.content {
                {
                    template = I.MWUI.templates.padding,
                    alignment = ui.ALIGNMENT.Center,
                    content = ui.content {
                        imageContent(buttonNormalResources[value], value)
                    },
                    props = {
                    },
                }
            }
        }
        table.insert(contentItems, newButton)
    end
    local element = ui.create {
        layer = "MainMenu",
        
            type = ui.TYPE.Container,
          --  template = I.MWUI.templates.box,
            content = ui.content(contentItems),
            events = {
              --  mouseMove = async:callback(mouseMove),
              --  focusLoss = async:callback(focusLoss),
               -- mousePress = async:callback(mousePress),
              --  mouseRelease = async:callback(mouseRelease),
            },
            props = {
                position = util.vector2(layerSize.x / 2, layerSize.y - 12 ),
                anchor = util.vector2(0.5, 1),
                
            }
        }
end
local function onLoad()
    if menu.getState() == menu.STATE.NoGame then
        drawButtons()
    end
end
local function onInit()
    if menu.getState() == menu.STATE.NoGame then
        drawButtons()
        --  loadMostRecent()
    end
end
local function hitEscKey()
    for index, button in ipairs(buttonUIs) do
        if button.layout.id == "menu_continue" then
            if button.layout.content[1].content[1].props.resource == buttonOverResources["menu_continue"] then
                destroyButtons()
                loadMostRecent()
                return
            end
            button.layout.content[1].content[1].props.resource = buttonOverResources["menu_continue"]
            button:update()
        end
    end
end
return
{
    interfaceName = "Continue",
    interface = {
        version = 1,
        drawButton = drawButton,
        getcontinueButtonNormal = function()
            return continueButtonNormal
        end
    },
    engineHandlers = {
        onLoad = onLoad,
        onInit = onInit,
        onConsoleCommand = onConsoleCommand,
        onKeyPress = function(key)
            if menu.getState() == menu.STATE.NoGame then
                if key.code == input.KEY.Escape then
                    hitEscKey()
                end
            end
        end,
        onStateChanged = function()
            if menu.getState() ~= menu.STATE.NoGame then
                destroyButtons()
                --  loadMostRecent()
            end
        end,
        onControllerButtonPress = function(code)
            if menu.getState() == menu.STATE.NoGame then
                if code == input.CONTROLLER_BUTTON.Start then
                    hitEscKey()
                end
            end
        end
    }
}
